//------------ << Step 1: Bibili Name >> ------------

let bibiliName = " ";
const bibiliNameSpan = document.getElementById('bibiliName');
const bibiliNameInput = bibiliNameSpan.parentElement.nextElementSibling;
bibiliNameInput.addEventListener('change', function(){
	bibiliName = bibiliNameInput.value;
	bibiliNameSpan.textContent = bibiliName.toUpperCase();
});

//------------ << Reset button >> ------------
const resetBtn = document.getElementById('resetBtn');

resetBtn.addEventListener('click', function() {
	document.getElementById('nagpapasaBuy').value = "";
	document.getElementById('itemName').value = "";
	document.getElementById('description').value = "";
	document.getElementById('quantity').value = "";
});

//------------ << Step 2: PasaBuy Input >> ------------
const pabiList = [];
let itemId = 1; 
var amount;

function updatePabiListTable(){
	pabiList.forEach(function(indivPabiList, dataIndex){
		const newRow = document.createElement('tr');
		newRow.innerHTML = 
		`<td>${indivPabiList.id}</td>
		<td>${indivPabiList.name}</td>
		<td>${indivPabiList.item}</td>
		<td>${indivPabiList.desc}</td>
		<td>${indivPabiList.qty}</td>
		<td>${indivPabiList.price}</td>
		<td><button class="btn btn-danger deleteBtn" data-id="${dataIndex}">Remove</button></td>
		<td><button class="btn btn-success paidBtn">Paid</button></td>`
		document.getElementById('pabiListDetails').appendChild(newRow);
		selectedRowToInputPrice();
	});
};

const addToTable = document.getElementById('mainPasaBuy').lastElementChild;
addToTable.addEventListener('click', function(){
	document.getElementById('pabiListDetails').innerHTML = "";
	const nagpapasaBuyName = document.getElementById('nagpapasaBuy').value;
	const itemName = document.getElementById('itemName').value;
	const description = document.getElementById('description').value;
	const quantity = document.getElementById('quantity').value
	// const price = document.getElementById('itemId').nextElementSibling;
	const mainPasaBuy = {
		id: itemId,
		name: nagpapasaBuyName,
		item: itemName,
		desc: description,
		qty: quantity,
		price: amount,
	};
	itemId++
	pabiList.push(mainPasaBuy);
	updatePabiListTable();
});


//------------ << Step 3 : Price update by Bibili >> ------------
const itemIdInput = document.getElementById('itemId');
const priceInput = itemIdInput.nextElementSibling;

let selectRowIndex;
var tb = document.getElementById('pabiListDetails')
function selectedRowToInputPrice(){
	
	var tbLength = tb.rows.length;
	for(var i = 0; i < tbLength; i++){
		tb.rows[i].onclick = function(){
			console.log(this.rowIndex);
			// alert(this.rowIndex);
			selectRowIndex = this.rowIndex;
			document.getElementById('itemId').value = this.cells[2].innerHTML;
		};
	}
}
selectedRowToInputPrice();

const aBtn = document.getElementById('acceptBtn');


aBtn.addEventListener('click', function(){
	var iPrice = document.getElementById('inputPrice').value;
	tb.rows[selectRowIndex-1].cells[5].innerHTML = iPrice;

})

document.addEventListener('click', function(event){
	if(event.target.classList.contains('paidBtn')&& tb.rows[selectRowIndex-1].cells[5].innerHTML){
		event.target.parentElement.parentElement.classList.add('bg-success');
	}
	if(event.target.classList.contains('deleteBtn')){
		document.getElementById('pabiListDetails').innerHTML = "";
		const index = event.target.getAttribute('data-id');
		pabiList.splice(index, 1);
		updatePabiListTable();
		
	}

});


